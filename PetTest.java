package org.example.Lab3;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.builder.RequestSpecBuilder;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.Map;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;


public class PetTest {
    private static final String baseUrl = "https://petstore.swagger.io/v2";
    private static final String PET = "/pet",
            PET_ID = PET + "/{petId}";

    private String petname;
    private Integer petid;
    private String[] photourls = {"E:\\ATVD\\testimg.jpg"};



    @BeforeClass
    public void setup()
    {
        RestAssured.baseURI = baseUrl;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
    }

    @Test()
    public void verifyCreateAction()
    {
        petname = Faker.instance().animal().name();
        petid = Faker.instance().number().randomDigit();
        Map<String, ?> body = Map.of(
                "name", petname,
                "id", petid,
                "photoUrls", photourls);
        given().body(body).post(PET).then().statusCode(HttpStatus.SC_OK);
        System.out.println(body);
    }

    @Test(dependsOnMethods = "verifyCreateAction")
    public void verifyGetAction()
    {
        given().pathParam("petId", petid)
                .get(PET_ID)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .and()
                .body("id", equalTo(petid));
    }

    @Test(dependsOnMethods = "verifyGetAction")
    public  void  verifyDeleteAction()
    {
        Map<String, ?> body = Map.of(
                "name", "Sharik",
                "id", petid,
                "photoUrls", photourls);
        given().body(body).post(PET).then().statusCode(HttpStatus.SC_OK).and().body("name", equalTo("Sharik"));
    }
}
