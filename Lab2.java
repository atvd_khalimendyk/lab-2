package org.example;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class Lab2
{
    private WebDriver chromeDriver;
    private static final String baseUrl = "https://uainfo.org/";

    @BeforeClass(alwaysRun = true)
    public void setUp()
    {
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--start-fullscreen");
        chromeOptions.setImplicitWaitTimeout(Duration.ofSeconds(15));
        chromeOptions.addArguments("--remote-allow-origins=*");
        this.chromeDriver = new ChromeDriver(chromeOptions);
    }

    @BeforeMethod
    public void preconditions()
    {
        chromeDriver.get(baseUrl);
    }

    /*@Test
    public void testClickOn()
    {
        WebElement forButton = chromeDriver.findElement(By.xpath("/html/body/div[1]/div/ul[1]/li[4]/a"));
        Assert.assertNotNull(forButton);
        forButton.click();
        Assert.assertNotEquals(chromeDriver.getCurrentUrl(), baseUrl);
    }*/

    /*@Test
    public void testHeaderExists()
    {
        WebElement header = chromeDriver.findElement(By.id("top-nav"));
        Assert.assertNotNull(header);
    }*/

    @Test
    public void testSearchFieldOnStudentPage()
    {
        String PageUrl = "https://uainfo.org/";
        chromeDriver.get(PageUrl);
        WebElement searchField = chromeDriver.findElement(By.tagName("input"));
        Assert.assertNotNull(searchField);
        System.out.println(String.format("Name attribute: %s", searchField.getAttribute("name")) +
                String.format("\nID attribute: %s", searchField.getAttribute("id")) +
                String.format("\nType attribute: %s", searchField.getAttribute("type")) +
                String.format("\nValue attribute: %s", searchField.getAttribute("value")) +
                String.format("\nPosition: (%d;%d)", searchField.getLocation().x, searchField.getLocation().y) +
                String.format("\nSize: (%dx%d)", searchField.getSize().height, searchField.getSize().width));

        String inputValue = "USA";
        searchField.sendKeys(inputValue);
        Assert.assertEquals(searchField.getText(), inputValue);
        searchField.sendKeys(Keys.ENTER);
        Assert.assertNotEquals(chromeDriver.getCurrentUrl(), PageUrl);
    }

    /*@Test
    public void testSlider()
    {
        WebElement nextButton = chromeDriver.findElement(By.className("flex-next"));
        WebElement contrknext = chromeDriver.findElement(By.xpath("/html/body/div[4]/section/div/div/ol[3]/li[2]/a"));
        WebElement contrprev = chromeDriver.findElement(By.xpath("/html/body/div[4]/section/div/div/ol[1]/li[1]/a"));

        //Assert.assertEquals(nextButton, nextButtonByCss);
        WebElement previousButton = chromeDriver.findElement(By.className("flex-prev"));

        for(int i = 0; i < 20; i++)
        {
            if(contrknext.getAttribute("class").isEmpty())
            {
                previousButton.click();
                Assert.assertTrue(!contrprev.getAttribute("class").isEmpty());
                Assert.assertFalse(contrknext.getAttribute("class").isEmpty());
            }
            else
            {
                nextButton.click();
                Assert.assertTrue(!contrknext.getAttribute("class").isEmpty());
                Assert.assertFalse(contrprev.getAttribute("class").isEmpty());
            }
        }
    }*/




    @AfterClass(alwaysRun = true)
    public void tearDown()
    {
        chromeDriver.quit();
    }
}

